class TrainPolicy < UserOwnedResourcePolicy
  def permitted_attributes
    %i(name capacity engine set_label)
  end
end
