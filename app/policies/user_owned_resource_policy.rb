# Default policy for user-owned resources.
#
# * Everyone can read everything
# * Non-guests can create resources and update/destroy their own resources
# * Admin can update/destroy everything
#
class UserOwnedResourcePolicy < ApplicationPolicy

  def index?
    true
  end

  def create?
    !guest?
  end

  def update?
    admin? || owns_resource?
  end

  def destroy?
    update?
  end

  def show?
    true
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end

  protected

  def owns_resource?
    record.user == user if record.respond_to? :user
  end
end
