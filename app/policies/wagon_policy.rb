class WagonPolicy < UserOwnedResourcePolicy
  def permitted_attributes
    %i(name capacity)
  end
end
