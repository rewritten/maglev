module Authentication
  extend ActiveSupport::Concern

  def current_user
    @current_user ||= User.find_by(username: request.env['REMOTE_USER'])
  end

end
