class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  include Authentication
  include Pundit

  rescue_from Pundit::NotAuthorizedError do head :not_authorized end
  rescue_from ActiveRecord::RecordNotFound do head :not_found end
end
