class TrainsController < ApplicationController
  def index
    authorize Train
    render json: { trains: Train.all }
  end

  def create
    @train = current_user.trains.build
    authorize @train
    @train.attributes = permitted_attributes(@train)
    @train.save!
    render json: { trains: [@train] }, status: :created
  end

  def update
    @train = Train.find(params[:id])
    authorize @train
    @train.attributes = permitted_attributes(@train)
    @train.save!
    render json: { trains: [@train] }, status: :ok
  end

  def show
    @train = Train.find(params[:id])
    authorize @train
    render json: { trains: [@train] }
  end

  def destroy
    @train = Train.find(params[:id])
    authorize @train
    @train.destroy
    head :no_content
  end

end
