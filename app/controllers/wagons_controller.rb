class WagonsController < ApplicationController
  def index
    load_train
    authorize Wagon
    render json: { wagons: @train.wagons }
  end

  def create
    load_train
    authorize @train, :update?
    @wagon = @train.wagons.build
    authorize @wagon
    @wagon.attributes = permitted_attributes(@wagon)
    @wagon.save!
    render json: { wagons: [@wagon] }, status: :created
  end

  def update
    load_wagon
    authorize @wagon
    @wagon.attributes = permitted_attributes(@wagon)
    @wagon.save!
    render json: { wagons: [@wagon] }, status: :ok
  end

  def show
    load_wagon
    authorize @wagon
    render json: { wagons: [@wagon] }
  end

  def destroy
    load_wagon
    authorize @wagon
    @wagon.destroy
    head :no_content
  end

  private

  def load_train
    @train = Train.find(params[:train_id])
  end

  def load_wagon
    @wagon = Wagon.find(params[:id])
  end
end
