class StatusController < ApplicationController
  def ping
    render json: { status: "ok",
                   response: "pong",
                   server_status: "no idea",
                   you: current_user.username }
  end
end
