class Wagon < ActiveRecord::Base
  belongs_to :train
  has_one :user, through: :train
end
