class Train < ActiveRecord::Base
  belongs_to :user
  has_many :wagons

  module Engines
    ALL = [
      ELECTRIC = "electric",
      MAGLEV = "maglev",
      STEAM = "steam",
      DIESEL = "diesel",
    ]
  end
end
