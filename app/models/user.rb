class User < ActiveRecord::Base
  has_secure_password

  has_many :trains
  has_many :wagons, through: :trains

  module Roles
    ALL = [
      GUEST = "guest",
      USER = "user",
      ADMIN = "admin",
    ]
  end
end
