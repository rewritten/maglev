class AuthenticationService

  # return the user if the username exists and the password is correct
  # otherwise return nil
  def self.authenticate(username, password)
    User.find_by(username: username).try(:authenticate, password)
  end

end
