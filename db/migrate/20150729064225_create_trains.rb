class CreateTrains < ActiveRecord::Migration
  def change
    create_table :trains do |t|
      t.string :name
      t.integer :capacity
      t.string :engine
      t.string :set_label
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
