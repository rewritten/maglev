class CreateWagons < ActiveRecord::Migration
  def change
    create_table :wagons do |t|
      t.string :name
      t.string :cargo
      t.integer :capacity
      t.references :train, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
