require "rails_helper"

RSpec.describe AuthenticationService do
  describe "authentication" do
    let!(:user) { User.create!(username: "john", password: "secret") }
    specify { expect(AuthenticationService.authenticate("john", "secret")).to be_a User }
  end
end
