require "rails_helper"

RSpec.describe WagonsController, type: :controller do

  let(:user) { User.create!(username: "john", password: "secret", role: User::Roles::USER) }
  let!(:train) {
    user.trains.create!(
      name: "Renfe Ave 103",
      capacity: 65,
      engine: Train::Engines::ELECTRIC
    )
  }
  let!(:wagon) {
    train.wagons.create!(
      name: "Ave 2nd Class",
      capacity: 595
    )
  }

  before do
    allow(controller).to receive(:current_user) { user }
  end

  describe "index" do
    before { get :index, train_id: train.id }
    specify { expect(response.body).to include "Ave 2nd Class" }
    specify { expect(train.wagons.count).to eq 1 }
    specify { expect(user.wagons.count).to eq 1 }
  end

  describe "create" do
    before { post :create, train_id: train.id, wagon: { name: "Ave Tail", capacity: 595 } }
    specify { expect(train.wagons.count).to eq 2 }
    specify { expect(user.wagons.count).to eq 2 }
  end

  describe "update" do
    before { put :update, id: wagon.id, wagon: { capacity: 780 } }
    specify { expect(wagon.reload.capacity).to eq 780 }
  end

  describe "show" do
    before { get :show, id: wagon.id }
    specify { expect(response.body).to include "Ave 2nd Class" }
  end
end
