require "rails_helper"

RSpec.describe TrainsController, type: :controller do

  let(:user) { User.create!(username: "john", password: "secret", role: role) }
  let(:role) { User::Roles::USER }

  let(:other_user) { User.create!(username: "john", password: "secret", role: "user") }

  let!(:train) {
    user.trains.create!(
      name: "Renfe Ave 103",
      capacity: 65,
      engine: Train::Engines::ELECTRIC
    )
  }

  let!(:other_train) {
    other_user.trains.create!(
      name: "SNCF TGV",
      capacity: 72,
      engine: Train::Engines::ELECTRIC
    )
  }

  before { allow(controller).to receive(:current_user) { user } }

  describe "index" do
    before { get :index }
    specify { expect(response.body).to include "Renfe Ave 103" }
    specify { expect(response).to be_success }
    specify { expect(user.trains.count).to eq 1 }
  end

  describe "create" do
    before { post :create, train: { name: "GP38AC Eagle", capacity: 28, engine: Train::Engines::DIESEL } }
    specify { expect(response).to be_success }
    specify { expect(user.trains.count).to eq 2 }
  end

  describe "update" do
    before { put :update, id: train.id, train: { capacity: 63 } }
    specify { expect(response).to be_success }
    specify { expect(train.reload.capacity).to eq 63 }
  end

  describe "update another train" do
    before { put :update, id: other_train.id, train: { capacity: 128 } }
    specify { expect(response).to be_error }
    specify { expect(other_train.reload.capacity).not_to eq 128 }
  end

  describe "show" do
    before { get :show, id: train.id }
    specify { expect(response).to be_success }
    specify { expect(response.body).to include "Renfe Ave 103" }
  end

  describe "admin user" do
    let(:role) { User::Roles::ADMIN }

    describe "update another train" do
      before { put :update, id: other_train.id, train: { capacity: 128 } }
      specify { expect(response).to be_success }
      specify { expect(other_train.reload.capacity).to eq 128 }
    end
  end

  describe "guest user" do
    let(:role) { nil }
    describe "create" do
      before { post :create, train: { name: "GP38AC Eagle", capacity: 28, engine: Train::Engines::DIESEL } }
      specify { expect(response).to be_error }
    end
  end
end
