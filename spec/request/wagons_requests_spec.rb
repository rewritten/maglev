require "rails_helper"

RSpec.describe "/wagons", type: :request do

  let(:user) { User.create!(username: "john", password: "secret", role: role) }
  let(:role) { User::Roles::USER }

  let(:other_user) { User.create!(username: "otherjohn", password: "othersecret", role: User::Roles::USER) }

  let!(:train) {
    user.trains.create!(
      name: "Renfe Ave 103",
      capacity: 65,
      engine: Train::Engines::ELECTRIC
    )
  }
  let!(:wagon) {
    train.wagons.create!(
      name: "Ave 2nd Class",
      capacity: 595
    )
  }

  let!(:other_train) {
    other_user.trains.create!(
      name: "SNCF TGV",
      capacity: 72,
      engine: Train::Engines::ELECTRIC
    )
  }
  let!(:other_wagon) {
    other_train.wagons.create!(
      name: "TGV Restaurant",
      capacity: 20
    )
  }

  it "GET /trains/:train_id/wagons" do
    expect {
      get "/trains/#{train.id}/wagons",
          {},
          credentials_for(user)
      expect(response.body).to include("Ave 2nd Class")
      expect(response).to be_success
    }.not_to change { user.wagons.count }
  end

  describe "POST /trains/:train_id/wagons" do
    it "normal user" do
      expect {
        post "/trains/#{train.id}/wagons",
             {wagon: { name: "GP38AC Eagle", capacity: 28 }},
             credentials_for(user)
        expect(response).to be_success
      }.to change { user.wagons.count }.by(1)
    end

    describe "as guest user" do
      let(:role) { nil }

      it "cannot create wagons" do
        expect {
          post "/trains/#{train.id}/wagons",
               {wagon: { name: "GP38AC Eagle", capacity: 28 }},
               credentials_for(user)
          expect(response).to be_error
        }.not_to change { Train.count }
      end
    end
  end

  describe "PUT /wagons/:id" do
    it "owned wagon - changed" do
      expect {
        put "/wagons/#{wagon.id}",
            {wagon: { capacity: 63 }},
            credentials_for(user)
        expect(response).to be_success
      }.to change { wagon.reload.capacity }.to(63)
    end

    it "other wagon - not changed" do
      expect {
        put "/wagons/#{other_wagon.id}",
            {wagon: { capacity: 128 }},
            credentials_for(user)
        expect(response).to be_error
      }.not_to change { other_wagon.reload.capacity }
    end

    describe "as admin" do
      let(:role) { User::Roles::ADMIN }

      it "other wagon - changed" do
        expect {
          put "/wagons/#{other_wagon.id}",
              {wagon: { capacity: 128 }},
              credentials_for(user)
          expect(response).to be_success
        }.to change { other_wagon.reload.capacity }.to(128)
      end
    end
  end

  describe "DELETE /wagons/:id" do
    it "owned wagon - deleted" do
      expect {
        delete "/wagons/#{wagon.id}",
               {},
               credentials_for(user)
        expect(response).to be_success
      }.to change { Wagon.count }.by(-1)
    end

    it "other wagon - not deleted" do
      expect {
        delete "/wagons/#{other_wagon.id}",
               {},
               credentials_for(user)
        expect(response).to be_error
      }.not_to change { Wagon.count }
    end

    describe "as admin" do
      let(:role) { User::Roles::ADMIN }

      it "other wagon - deleted" do
        expect {
          delete "/wagons/#{other_wagon.id}",
                 {},
                 credentials_for(user)
          expect(response).to be_success
        }.to change { Wagon.count }.by(-1)
      end
    end
  end



  it "GET /wagons/:id" do
    get "/wagons/#{wagon.id}", {}, credentials_for(user)
    expect(response).to be_success
    expect(response.body).to include "Ave 2nd Class"
  end



  # describe "admin user" do
  #   Given(:role) { User::Roles::ADMIN }

  #   describe "update another train" do
  #     When { put :update, id: other_train.id, train: { capacity: 128 } }
  #     Then { response.redirect? }
  #     Then { other_train.reload.capacity == 128 }
  #   end
  # end

  # describe "guest user" do
  #   Given(:role) { nil }
  #   describe "create" do
  #     When { post :create, train: { name: "GP38AC Eagle", capacity: 28, engine: Train::Engines::DIESEL } }
  #     Then { response.error? }
  #     # Then { byebug }
  #   end
  # end
end
