require "rails_helper"

RSpec.describe "/trains", type: :request do

  let(:user) { User.create!(username: "john", password: "secret", role: role) }
  let(:role) { User::Roles::USER }

  let(:other_user) { User.create!(username: "john", password: "secret", role: "user") }

  let!(:train) {
    user.trains.create!(
      name: "Renfe Ave 103",
      capacity: 65,
      engine: Train::Engines::ELECTRIC
    )
  }

  let!(:other_train) {
    other_user.trains.create!(
      name: "SNCF TGV",
      capacity: 72,
      engine: Train::Engines::ELECTRIC
    )
  }

  it "GET /trains" do
    expect {
      get "/trains", {}, credentials_for(user)
      expect(response.body).to include("Renfe Ave 103")
      expect(response).to be_success
    }.not_to change { user.trains.count }
  end

  describe "POST /trains" do
    it "normal user" do
      expect {
        post "/trains", {train: { name: "GP38AC Eagle", capacity: 28, engine: Train::Engines::DIESEL }}, credentials_for(user)
        expect(response).to be_success
      }.to change { user.trains.count }.by(1)
    end
    describe "as guest user" do
      let(:role) { nil }

      it "cannot create trains" do
        expect {
          post "/trains", {train: { name: "GP38AC Eagle", capacity: 28, engine: Train::Engines::DIESEL }}, credentials_for(user)
          expect(response).to be_error
        }.not_to change { Train.count }
      end
    end
  end

  describe "PUT /trains/:id" do
    it "owned train - changed" do
      expect {
        put "/trains/#{train.id}", {train: { capacity: 63 }}, credentials_for(user)
        expect(response).to be_success
      }.to change { train.reload.capacity }.to(63)
    end

    it "other train - not changed" do
      expect {
        put "/trains/#{other_train.id}", {train: { capacity: 128 }}, credentials_for(user)
        expect(response).to be_error
      }.not_to change { other_train.reload.capacity }
    end

    describe "as admin" do
      let(:role) { User::Roles::ADMIN }

      it "other train - changed" do
        expect {
          put "/trains/#{other_train.id}", {train: { capacity: 128 }}, credentials_for(user)
          expect(response).to be_success
        }.to change { other_train.reload.capacity }.to(128)
      end
    end
  end

  describe "DELETE /trains/:id" do
    it "owned train - deleted" do
      expect {
        delete "/trains/#{train.id}", {}, credentials_for(user)
        expect(response).to be_success
      }.to change { Train.count }.by(-1)
    end

    it "other train - not deleted" do
      expect {
        delete "/trains/#{other_train.id}", {}, credentials_for(user)
        expect(response).to be_error
      }.not_to change { Train.count }
    end

    describe "as admin" do
      let(:role) { User::Roles::ADMIN }

      it "other train - deleted" do
        expect {
          delete "/trains/#{other_train.id}", {}, credentials_for(user)
          expect(response).to be_success
        }.to change { Train.count }.by(-1)
      end
    end
  end



  it "GET /trains/:id" do
    get "/trains/#{train.id}", {}, credentials_for(user)
    expect(response).to be_success
    expect(response.body).to include "Renfe Ave 103"
  end



  # describe "admin user" do
  #   Given(:role) { User::Roles::ADMIN }

  #   describe "update another train" do
  #     When { put :update, id: other_train.id, train: { capacity: 128 } }
  #     Then { response.redirect? }
  #     Then { other_train.reload.capacity == 128 }
  #   end
  # end

  # describe "guest user" do
  #   Given(:role) { nil }
  #   describe "create" do
  #     When { post :create, train: { name: "GP38AC Eagle", capacity: 28, engine: Train::Engines::DIESEL } }
  #     Then { response.error? }
  #     # Then { byebug }
  #   end
  # end
end
