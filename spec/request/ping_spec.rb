require "rails_helper"

RSpec.describe "Status", type: :request do
  let(:user) { User.create!(username: "john", password: "secret") }

  describe "GET #ping - no credentials" do
    it "responds with a 401 error" do
      get "/ping"
      expect(response).not_to be_success
      expect(response.status).to eq 401
    end
  end

  describe "GET #ping - valid credentials" do
    it "responds with success" do
      get "/ping", {}, credentials_for(user)
      expect(response).to be_success
      expect(response.body).to match /"pong"/
    end
  end

end
