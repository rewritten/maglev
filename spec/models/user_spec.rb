require "rails_helper"

RSpec.describe User, type: :model do
  describe "basic features" do
    let(:user) { User.new }
    specify { expect(user).to respond_to :username }
    specify { expect(user).to respond_to :password }
  end
end
