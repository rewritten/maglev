module AuthenticationHelpers
  def credentials_for(user)
    {
      'HTTP_AUTHORIZATION' => ActionController::HttpAuthentication::Basic.encode_credentials(user.username, user.password)
    }
  end
end
